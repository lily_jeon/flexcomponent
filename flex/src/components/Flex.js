import React from 'react'

export default function Flex(props) { 
    const align = props.align;
    const direction = props.direction;
    const split = props.split;
    const children = props.children;

    const style = {
        display : 'flex',
        alignItems : align,
        flexDirection : direction,
        border : '1px solid',
        borderColor: '#56d0ff',

        margin: '10px',
        padding: '20px',
        width: '50%',
        justifyContent: 'space-around'
    }

    const setSplit = () => {
        let tmp = [];
        tmp.push(children[0]);
        
        for(let i = 1; i < children.length; i++) {
            tmp.push(split);
            tmp.push(children[i]);
        }

        return tmp;
    }

    return (
        <div style={style}>
            { setSplit() } 
        </div>
    )
}
