import './App.css';
import Flex from './components/Flex.js';

function App() {
  const align = 'center';
  const direction = 'row';
  const reactElement = <div>|</div>

  const btn = {
    backgroundColor : '#56d0ff',
    padding: '10px',
    color: 'white',
    border: 'none',
    borderRadius: '0.5rem',
    margin: '0 1rem 0 1rem'
  }

  const block = {
    display: 'flex',
    padding: '10px',
    backgroundColor: '#c7c7c7',
    height: 100,
    alignItems: 'center',
    justifyContent: 'space-around',
    width: '100%',
    margin: '10px'
  }

  return (
    <div className="App">
      <Flex align={align} direction={direction}  split={null}>
        <div>{align}</div>
        <div>
          <button style={btn}>Primary</button>
        </div>
        <div style={block}>Block</div>
      </Flex>
      <Flex align={align} direction={direction} split={reactElement}>
        <div>Link</div>
        <div>Link</div>
        <div>Link</div>
      </Flex>   
    </div>
  );
}

export default App;
